const express = require('express');
const queue = require('queue');

const Manager = require('./server-lib/task-manager');
const projects = `${ __dirname }/data/start.json`;

const app = express();
const manager = new Manager({ dataWay: `${ __dirname }/data/` });

const origins = ["http://localhost:3000", "http://localhost:5000"];

const runQueue = queue({ autostart: true, concurrency: 1 });
const { getRandomTask } = require('./server-lib/task-manager/getRandomTask')

app.use(express.json());
app.use(express.urlencoded());

app.use((req, res, next) => {
  const { origin } = req.headers;
  origins.filter(item => item === origin)
    && res.setHeader('Access-Control-Allow-Origin', origin);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.get('/data/projects', (req, res) => {
  res.send(manager.getProjects());
});

app.get('/data/item/:project/:env', (req, res) => {
  const { project, env } = req.params;
  res.send(manager.getItem(project, env));
});

app.post('/data/add-project', (req, res) => {
  const props = req.body;
  const project = { 
    name: props.name, 
    environments: {
      int: props.int,
      qa: props.qa,
      staging: props.staging,
      production: props.production
    } 
  }
  manager.addProject(project);
  res.send(manager.getProject(props.name));
});

app.post('/data/remove-tasks', (req, res) => {
  const { name } = req.body;
  res.send({ result: manager.removeProject(name) });
});

const queryTick = (name, env) => new Promise((res, rej) => {
  const task = getRandomTask(name, env);
  const { duration } = task;
  task.duration = `${ Math.ceil(duration/1000) }s`;
  setTimeout(() => {
    manager.refreshTask(name, env, task)
    res(name, env);
  }, duration)
});

app.post('/data/refresh', (req, res) => {
  const { name, env } = req.body;
  manager.setQueue(name, env);
  runQueue.push(() => queryTick(name, env));
  res.send(manager.getTask(name, env));
});

app.get('/logo.txt', (req, res) => {
  res.send({ result: "This is LOGO.TXT" });
});

app.use('/', express.static(__dirname));

const port = 9999;

app.listen(port, () => console.log(`Express server listening on port ${ port }`));