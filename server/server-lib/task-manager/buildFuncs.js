const { TaskStatus } = require('./TaskStatus');

const emptyTask = {
  status: TaskStatus.MISSING,
  version: "v-.-.-", 
}

module.exports.buildTask = item => {
  const task = item || emptyTask;
  return { ...task }
}

module.exports.buildItem = (name, environment, data) => {
  const task = data || emptyTask;
  const { version, timestamps, duration } = task;
  const tests = task.testResult;
  const status = tests ? `${ tests.passed } of ${ tests.total }` : "";
  return {
    name,
    environment,
    name,
    environment,
    version,
    timestamps,
    duration,
    status 
  }
}