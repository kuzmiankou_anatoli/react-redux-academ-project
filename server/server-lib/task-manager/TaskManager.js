const fs = require('fs');
const { buildItem, buildTask } = require('./buildFuncs');
const { TaskStatus } = require('./TaskStatus');

class TaskManager {
  constructor(props) {
    this.state = {};
    this.dataWay = props.dataWay;
    require(`${ this.dataWay }start.json`)
      .forEach( item => this.addProject(item) );
  }

  createTask(way) {
    const path = `${ this.dataWay }${ way }aura.json`;
    const task = fs.existsSync(path) ? require(path) : null;
    return buildTask(task);
  }

  refreshTask(name, env, task) {
    this.state[name][env] = task;
  }

  getTask(name, env) {
    return this.state[name][env];
  }

  addProject(project) {
    const { name, environments } = project;
    const envs = {};
    Object.keys(environments)
      .filter(item => environments[item])
      .forEach(item => envs[item] = this.createTask(environments[item]));
    this.state[name] = envs;
  }

  getProject( name ) {
    return { name, environments: this.state[name] }
  }

  setQueue( name, env ) {
    this.state[name][env] = { status: TaskStatus.QUEUE };
  }

  setRunning( name, env ) {
    this.state[name][env] = { status: TaskStatus.RUNNING };
  }

  getProjects() {
    const { state } = this;
    return Object.keys(state).map(item => this.getProject(item));
  }

  getItem(name, environment) {
    return buildItem(name, environment, this.state[name][environment]);
  }

  removeProject(name) {
    return delete this.state[name];
  }
}

module.exports = TaskManager;