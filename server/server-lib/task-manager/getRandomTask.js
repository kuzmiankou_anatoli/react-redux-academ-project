const { TaskStatus } = require('./TaskStatus');

const createFailedResult = ans => {
  const total = ans%150;
  const failed = ans% total;
  return {
    total,
    failed,
    passed: total - failed,
    skipped: 0
  };
};

const createSuccessResult = ans => {
  const total = ans%150;
  return {
    total,
    failed: 0,
    passed: total,
    skipped: 0
  };
};

module.exports.getRandomTask = (name, environment) => {
  const date = new Date();
  const ans = date.getTime();
  const probability = Math.random();
  const result = {
    name,
    environment,
    version: "v1.0.0",
    timestamps: date.toISOString(),
    duration: ans%15000
  }
  if(probability < 0.6) {
    return {
      ...result,
      status: TaskStatus.FAILED,
      testResult: createFailedResult(ans)
    }
  }
  else if(probability < 0.85) {
    return {
      ...result,
      status: TaskStatus.W_O_FAILED,
      logo: "logo.txt"
    }
  }
  else {
    return {
      ...result,
      status: TaskStatus.SUCCESS,
      testResult: createSuccessResult(ans)
    }
  }
}