import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router'
import createHistory from 'history/createBrowserHistory';
import Header from './components/Header';
import Footer from './components/Footer';
import HomePage from './pages/HomePage';
import AboutMePage from './pages/AboutMePage';
import ItemPage from './pages/ItemPage';
import ErrorPage from './pages/ErrorPage';
import LogoPage from './pages/LogoPage';
import TaskManagerPage from './pages/TaskManagerPage';
import * as PropTypes from 'prop-types';

class App extends Component {
  static PAGE_LINKS = [
    { "link": "/task-manager", "title": "Task manager" },
    { "link": "/about", "title": "About me" }
  ];

  getChildContext() {
    return {
      pageLinks: App.PAGE_LINKS
    }
  }
  
  render() {
    return (
      <div className="app">
        <Header text="Front-end EPAM training" />
        <Router history={ createHistory() }>
          <Switch>
            <Route exact path="/" component={ HomePage } />
            <Route path="/about" component={ AboutMePage } />
            <Route path="/task-manager" component={ TaskManagerPage } />
            <Route path="/item/:name/:environment" component={ ItemPage } />
            <Route path="/logo.txt" component={ LogoPage }/>
            <Route path="/*" component={ ErrorPage } />
          </Switch>
        </Router>
        <Footer date="2017" title="Kuzmiankou Anatoli" />
      </div>
    );
  }

  static childContextTypes = {
    pageLinks: PropTypes.arrayOf(PropTypes.shape({
      link: PropTypes.string,
      title: PropTypes.string
    }))
  }
}

export default App;