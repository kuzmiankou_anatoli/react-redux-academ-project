import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import NavBar from './NavBar';
import Loading from './Loading';

class Main extends Component {
  getChildContext() {
    const { name } = this.props;
    return {
      name
    }
  }

  render() {
    const { loading, children } = this.props;
    return (
      <main>
        <NavBar />
        {
          (loading)
            ? <Loading />
            : <div className="main-content">{ children }</div>
        }
      </main>
    );
  }

  static childContextTypes = {
    name: PropTypes.string,
    loading: PropTypes.bool
  };
}

export default Main;