import React from 'react';
import * as PropTypes from 'prop-types';

const Counter = props => {
  const { name, title, count } = props;
  return (
    <figure className={ `counter counter-${ name }` }>
      { `${ title } (${ count })` }
    </figure>
  );
}

Counter.defaultProps = {
  name: "counter",
  count: 0  
};

Counter.propTypes = {
  name: PropTypes.string,
  count: PropTypes.number,
  title: PropTypes.string
};

export default Counter;