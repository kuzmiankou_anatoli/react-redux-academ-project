import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

configure({ adapter: new Adapter() });

import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import TextField from '../fields/TextField';

describe('Testing Counter component', () => {
  it('renders without crashing', () => {
    mount(<TextField />);
  });
  describe('count of elements', () => {
    const wrapper = mount(<TextField />);
    it('label', () => {
      expect(wrapper.find('label').length).toBe(1);
    });
    it('input', () => {
      expect(wrapper.find('label').length).toBe(1);
    });
  });
});