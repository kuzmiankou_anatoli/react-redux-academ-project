import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

configure({ adapter: new Adapter() });

import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Counter from '../Counter';

describe('Testing Counter component', () => {
  it('renders without crashing', () => {
    shallow(<Counter />);
  });
  it('testing children count', () => {
    const wrapper = shallow(<Counter title="Counter" count={ 3 } />);
    expect(wrapper.children().length).toBe(1);
  });
  it("testing component's class name", () => {
    const wrapper = shallow(<Counter title="Counter" count={ 3 } />);
    expect(wrapper.props().className).toBe('counter counter-counter');
    expect(wrapper.props().children).toBe('Counter (3)');
  });
});