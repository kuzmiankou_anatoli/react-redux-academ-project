import React, {Component} from 'react';
import * as PropTypes from 'prop-types';

class TableBox extends Component {

  createTH(name) {
    return (
      <th className="column-tittle" key={ name }>{ name }</th>
    );
  }

  render() {
    const { environments } = this.context;
    return (
      <table className="table-box">
        <thead>
          <tr>
            { this.createTH("Name") }
            { environments && environments.map( item => this.createTH(item.title) ) }
          </tr>
        </thead>
        { this.props.children }
      </table>
    )
  }

  static contextTypes = {
    environments: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string
    })),
  };
}

export default TableBox;