import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import autoBind from 'react-autobind';

class TextField extends Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  componentDidMount() {
    this.resetValues();
  }

  resetValues() {
    this.field.value = this.props.value || "";
    this.blur();
  }

  blur() {
    this._valid = this.isValid();
  }

  isValid() {
    return this.props.validators.every(item => item(this.field.value));
  }

  get validity() {
    return this._valid;
  }

  get value() {
    return this.field.value;
  }

  get name() {
    return this.props.name;
  }

  render() {
    const { type, name, id, placeholder, title, labelValue } = this.props;
    return (
      <div className={ "text-field text-field-" + name }>
        <label htmlFor={ this.props.id } className="text-field-title">{ labelValue }</label>
        <input
          type={ type }
          name={ name }
          id={ id }
          placeholder={ placeholder }
          onBlur={ this.blur }
          ref={ field => this.field = field }
          className="text-field-input"
          title={ title }
        />
      </div>
    );
  }

  static defaultProps = {
    type: "text",
    labelValue: "Input field",
    name: "text",
    validators: []  
  };

  static propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    type: PropTypes.oneOf(['text', 'email', 'search', 'tel', 'url']),
    labelValue: PropTypes.string,
    plaseholder: PropTypes.string,
    title: PropTypes.string,
    value: PropTypes.string,
    validators: PropTypes.array  
  }
};

export default TextField;