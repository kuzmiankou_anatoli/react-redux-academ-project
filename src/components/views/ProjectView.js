import React from 'react';
import SkillsView from './SkillsView';
import * as PropTypes from 'prop-types';

const Project = props => {
  const { title, skills, date, overview, link } = props.data;
  return (
    <div>
      <h4>{ title }</h4>
      <SkillsView values={ skills } />
      <div className="date">{ date }</div>
      <p>{ overview }
        <a className="blank-link" href={ link }>{ link }</a>
      </p>
    </div>
  );
}

Project.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    skills: PropTypes.arrayOf(PropTypes.string),
    date: PropTypes.string,
    overview: PropTypes.string,
    link: PropTypes.string
  })
};

export default Project;