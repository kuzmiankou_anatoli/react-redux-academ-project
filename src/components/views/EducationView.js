import React from 'react';
import * as PropTypes from 'prop-types';

const Education = props => {
  const { school, start, end, degree, study } = props.data;
  return (
    <div>
      <h4>{ school } </h4>
      <div> { `${ start } - ${ end || "NOW" }` } </div>
      <p>{ `${ degree } | ${ study }` }</p>
    </div>
  );
}

Education.propTypes = {
  data: PropTypes.shape({
    school: PropTypes.string,
    start: PropTypes.string,
    end: PropTypes.string,
    degree: PropTypes.string,
    study: PropTypes.string
  })
};

export default Education;