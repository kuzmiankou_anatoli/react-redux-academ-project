import React, { Component } from 'react';
import * as PropTypes from 'prop-types';

class Skills extends Component {
  createSkill(name) {
    return (
      <div key={ name } className="skill">{ name }</div>
    );
  }

  render() {
    return (
      <div className="skills">
        { this.props.values.map( item => this.createSkill(item) ) }
      </div>
    )
  }

  static PropTypes = {
    values: PropTypes.arrayOf(PropTypes.string)
  };

  static defaultProps = {
    values: []
  };
}

export default Skills;