import React from 'react';
import * as PropTypes from 'prop-types';

const Employment = props => {
  const { company, start, end, position } = props.data;
  return (
    <div>
      <h4>{ company }</h4>
      <div className="interval">{ `${ start } - ${ end || "NOW" }` }</div>
      <p>{ position }</p>
    </div>
  );
}

Employment.propTypes = {
  data: PropTypes.shape({
    company: PropTypes.string,
    start: PropTypes.string,
    end: PropTypes.string,
    position: PropTypes.string
  })
};

export default Employment;