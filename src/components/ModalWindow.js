import React, {Component} from "react";
import * as PropTypes from 'prop-types';
import autoBind from 'react-autobind';

class ModalWindow extends Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  componentDidMount() {
    if(this.props.startVisible) {
      this.show();
    }
    else {
      this.hide();
    }
  }      

  hide() {
    this.window.style.display = "none";
  }

  show() {
    this.window.style.display = "block";
  }

  render() {
    const { id, name, title, children } = this.props;
    return (
      <figure
        id={ id }
        name={ name } 
        className={ "modal modal-" + name } 
        ref={ window => this.window = window }
      >
        <div className="modal-content">
          <h2>{ title }</h2>
          <button className="btn-close" onClick={ this.hide }>x</button>
          { children }
        </div>
      </figure>
    );
  }

  static defaultProps = {
    name: "window",
    title: "Modal window",
    startVisible: false
  };
  
  static propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    title: PropTypes.string,
    startVisible: PropTypes.bool
  };
}

export default ModalWindow;