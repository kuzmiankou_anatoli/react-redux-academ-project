import React, {Component} from "react";
import * as BodyFactory from "./lib/TaskBodyFactory";
import * as StatusFactory from "./lib/TaskStatusFactory";
import TaskStatus from "./lib/TaskStatus";
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { refreshTask, refreshTaskSuccess } from '../../redux/action/TaskAction';
import autoBind from 'react-autobind';

class Task extends Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  refreshAction() {
    const { name, env } = this.props;
    this.props.refreshAction({ name, env });
  }

  shouldComponentUpdate(nextProps, nextState) {
    for(let index in this.props) {
      if(this.props[index] !== nextProps[index]) {
        return true;
      }
    }
    return false;
  }

  render() {
    const { env, link, data } = this.props;
    const { status, version } = this.props.data;
    return(
      <td className={ `task task-${ env } task-${ TaskStatus[status].toLowerCase() }` }>
        <div className="header">
          <div className="version">{ version }</div>
          { StatusFactory.createStatus(status) }
        </div>
        { BodyFactory.createBodyElem(data) }
        <div className="control-panel">
          <a href={ link } className="link" ><span className="icon-link" /></a>
          <button className="refresh icon-refresh" onClick={ this.refreshAction } />
        </div>
      </td>
    );
  }

  static defaultProps = {
    name: "PROJECT",
    env: "int",
    data: {
      version: "v-.-.-",
      status: TaskStatus.MESSING
    }
  }
  
  static propsTypes = {
    name: PropTypes.string,
    env: PropTypes.string,
    data: PropTypes.shape({
      version: PropTypes.string,
      status: PropTypes.number,
      timestamps: PropTypes.string,
      testResult: PropTypes.shape({
        total: PropTypes.number,
        failed: PropTypes.number,
        passed: PropTypes.number,
        skipped: PropTypes.number
      }),
      logo: PropTypes.string
    }),
    link: PropTypes.string
  };
}

const mapDispatchToProps = dispatch => ({ 
  refreshAction: params => {
    dispatch(refreshTask());
    fetch('http://localhost:9999/data/refresh', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params)
    }).then(res => res.json())
      .then(data => dispatch(refreshTaskSuccess(params, data)));
  } 
});

export default connect(null, mapDispatchToProps)(Task);