import React from 'react';
import Status from './TaskStatus';

const titles = {
  [Status.QUEUE]: "IN QUEUE",
  [Status.MISSING]: "MISSING AURA.JSON",
  [Status.RUNNING]: "RUNNING",
  [Status.FAILED]: "FAILED",
  [Status.W_O_FAILED]: "FAILED"
}

const icons = {
  [Status.SUCCESS]: "icon-ok",
  [Status.W_O_FAILED]: "icon-warning"
}

export const createStatus = status => (
  <div className={ `status ${ icons[status] }` }>{ titles[status] }</div>
);