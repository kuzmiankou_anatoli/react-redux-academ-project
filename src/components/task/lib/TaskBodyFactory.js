import React from 'react';
import Status from './TaskStatus';
import moment from 'moment';

const viewQueue = status => (
  <div className="inqueue" key="inqueue">In queue</div>
)

const viewTests = tests => (
  <div key="tests">
    { `${ tests.failed } failed, ${ tests.passed } passed, ${ tests.skipped } skipped, ${ tests.total } total` }
  </div>
);

const viewLogo = link => (
  <div key="link">See <a href={ `/${ link }` } target="_blank">{ link }</a></div>
);

const viewTime = time => (
  <div key="date">{ moment(time).fromNow() }</div>
);

export const createBodyElem = data => (
  <div className="body">
    { data.status === Status.QUEUE && viewQueue(data.status) }
    { data.testResult && viewTests(data.testResult) }
    { data.logo && viewLogo(data.logo) }
    { data.timestamps && viewTime(data.timestamps) }
  </div>
);