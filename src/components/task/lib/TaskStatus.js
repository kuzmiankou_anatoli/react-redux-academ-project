const TaskStatus = {
  QUEUE: 'QUEUE',
  MISSING: 'MISSING',
  RUNNING: 'RUNNING',
  FAILED: 'FAILED',
  SUCCESS: 'SUCCESS',
  W_O_FAILED: 'W_O_FAILED'
}

export default TaskStatus;