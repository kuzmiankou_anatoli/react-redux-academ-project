import React from 'react';
import * as PropTypes from 'prop-types';

const NavBar = (props, context) => {
  const { name } = context
  return (
    <nav className="nav-bar">
      <ul className="nav-bar-menu">
        <li className="horisontal"><a href="/">Home</a></li>
        { 
          (name) 
            ? <li className="horisontal">{ name }</li> 
            : undefined
        }
      </ul>
  </nav>
  );
}

NavBar.contextTypes = {
  name: PropTypes.string
};

export default NavBar;