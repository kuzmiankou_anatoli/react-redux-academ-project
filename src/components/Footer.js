import React from 'react';
import * as PropTypes from 'prop-types';

const Footer = props => {
  return (
    <footer>&copy; { props.year }, { props.title }</footer>
  );
}

Footer.defaultProps = {
  year: new Date().getFullYear(),
  title: "site"
};

Footer.propTypes = {
  year: PropTypes.number,
  title: PropTypes.string
};

export default Footer;