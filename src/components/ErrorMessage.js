import React from 'react';
import * as PropTypes from 'prop-types';

const ErrorMessage = props => {
  return (
    <section className="message error-message">
      <h2>Error</h2>
      <p>{ `${ props.number } - ${ props.message }` }</p>
    </section>
  );
}

ErrorMessage.defaultProps = {
  number: "404",
  message: "Page not found"
};

ErrorMessage.propTypes = {
  number: PropTypes.string,
  message: PropTypes.string
};

export default ErrorMessage;