import React, {Component} from 'react';
import TextField from "./fields/TextField.js";
import * as PropTypes from "prop-types";
import autoBind from 'react-autobind';

class TextForm extends Component {
  constructor(props) {
    super(props);
    this.fields = [];
    autoBind(this);
  }

  submitAction(e) {
    e && e.preventDefault();
    this.props.submitAction(this.values);
  }

  resetAction(e) {
    e && e.preventDefault();
    this.fields.forEach(item => item.resetValues());
  }

  get values() {
    return this.fields.reduce( (values, current) => { values[current.name] = current.value; return values } , {})
  }

  get validity() {
    return this.fields.every( item => item.validity );
  }

  isValid() {
    return this.fields.every( item => item.isValid );
  }

  addField(field) {
    this.fields.push(field);
  }

  createField(props) {
    return (
      <TextField 
        key={ props.name }
        labelValue={ props.labelValue } 
        name={ props.name } 
        ref={ this.addField } 
      />
    );
  }

  render() {
    return (
      <form 
        name={ this.props.name } 
        className={ `form form-${ this.props.name }` }
        onReset={ this.resetAction }
        onSubmit={ this.submitAction }
      >
        { this.context.fields.map(item => this.createField(item)) }
        <input type="submit" value={ this.props.submitName } />
        <input type="reset" value={ this.props.resetName } />
      </form>
    );
  }
  
  static defaultProps = {
    name: "add-component",
    submitName: "Submit",
    resetName: "Reset",
    submitAction: () => {}
  };

  static propTypes = {
    name: PropTypes.string,
    submitName: PropTypes.string,
    resetName: PropTypes.string,
    submitAction: PropTypes.func
  };

  static contextTypes = {
    fields: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      labelValue: PropTypes.string
    }))
  };
}

export default TextForm;