import React, {Component} from 'react';
import Task from './task/Task';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeProjectTasks, removeProjectTasksSuccess } from '../redux/action/TaskAction';
import autoBind from 'react-autobind';

class TaskComponentManager extends Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  createTask(key, task) {
    const name = this.props.taskName;
    return (
      <Task
        name={ name } 
        key={ key }
        env={ key } 
        data={ task } 
        link={ `/item/${ name }/${ key }` }
      />
    )
  }
  
  createEmptyTask(key) {
    return <td key={ key }><div className="task-empty" /></td>
  }

  createTasks(tasks) {
    return this.context.environments.map(item => {
      const name = item.name;
      const task = tasks[name];
      return task ? this.createTask(name, task) : this.createEmptyTask(name);
    });
  }

  removeComponent() {
    this.props.removeAction({ name: this.props.taskName });
  }

  render() {
    const { name, taskName, tasks } = this.props;
    return (
      <tbody className={ `task-component-manager manager-${ name }` }>
        <tr>
          <td className="task-manager">
            <h2 className="title">{ taskName }</h2>
            <ul className="settings icon-setting">
              <li onClick={ this.removeComponent }>Remove</li>
            </ul>
          </td>
          { this.createTasks(tasks) }
        </tr>
      </tbody>
    );
  }

  static defaultProps = {
    name: "task-manager"
  };

  static contextTypes = {
    environments: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      title: PropTypes.string
    })),
  };
  
  static propTypes = {
    name: PropTypes.string,
    taskName: PropTypes.string,
    tasks: PropTypes.objectOf(PropTypes.shape({
      version: PropTypes.string,
      status: PropTypes.string,
      testResult: PropTypes.shape({
        total: PropTypes.number,
        failed: PropTypes.number,
        passed: PropTypes.number,
        skipped: PropTypes.number
      }),
      timestatmps: PropTypes.string,
      logo: PropTypes.string
    }))
  };
}

const mapDispatchToProps = dispatch => ({ 
  removeAction: params => {
    dispatch(removeProjectTasks());
    fetch('http://localhost:9999/data/remove-tasks', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params)
    }).then(res => res.json())
      .then(data => dispatch(removeProjectTasksSuccess(params.name, data.result)));
  } 
});

export default connect(null, mapDispatchToProps)(TaskComponentManager);