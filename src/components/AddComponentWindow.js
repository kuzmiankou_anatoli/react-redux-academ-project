import React, {Component} from 'react';
import TextForm from './TextForm';
import ModalWindow from './ModalWindow';
import * as PropTypes from 'prop-types';
import { addProjectTasks, addProjectTasksSuccess } from '../redux/action/TaskAction';
import { connect } from 'react-redux';
import autoBind from 'react-autobind';

class AddComponentWindow extends Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }

  getChildContext() {
    const { environments } = this.context;
    const fields = [{ name: "name", labelValue: "Item name" }];
    environments && environments.forEach( item => 
      fields.push({ 
        name: item.name, 
        labelValue: `${ item.title } envirment url` 
      })
    );
    return { fields };
  }

  formAction(data) {
    this.hideWindow();
    this.props.addComponent(data);
    this.form.resetAction();
  }
  
  hideWindow() {
    this.modalWindow.hide();
  }

  showWindow() {
    this.modalWindow.show();
  }

  render() {
    return (
      <figure className="add-component" name="add-component">
        <button name="add-component" onClick={ this.showWindow }
          className="btn-add-component">Add Component</button>
        <ModalWindow ref={ window => this.modalWindow = window } title="Add component item">
          <TextForm ref={ form => this.form = form } submitName="Add" submitAction={ this.formAction }/>
        </ModalWindow>
      </figure>
    );
  }

  static contextTypes = {
    environments: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      title: PropTypes.string
    })),
  };
  
  static childContextTypes = {
    fields: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      labelValue: PropTypes.string
    }))
  };
}

const mapDispatchToProps = dispatch => ({
  addComponent: params => {
    dispatch(addProjectTasks(params.name));
    fetch('http://localhost:9999/data/add-project', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params)
    }).then(res => res.json())
      .then(json => dispatch(addProjectTasksSuccess(json)));
  }
});

export default connect(null, mapDispatchToProps)(AddComponentWindow);