import React from 'react';
import * as PropTypes from 'prop-types';

export const Header = props => {
  return (
    <header>
      <h1> { props.text } </h1>
    </header>
  );
}

Header.propTypes = {
  text: PropTypes.string
};

export default Header;