export const LOAD_TASKS = 'LOAD_TASKS';
export const LOAD_TASKS_SUCCESS = 'LOAD_TASKS_SUCCESS';
export const ADD_PROJECT_TASKS = 'ADD_PROJECT_TASKS';
export const ADD_PROJECT_TASKS_SUCCESS = 'ADD_PROJECT_TASKS_SUCCESS';
export const REFRESH_TASK = 'REFRESH_TASK';
export const REFRESH_TASK_SUCCESS = 'REFRESH_TASK_SUCCESS';
export const REMOVE_PROJECT_TASKS = 'REMOVE_PROJECT_TASKS';
export const REMOVE_PROJECT_TASKS_SUCCESS = 'REMOVE_PROJECT_TASKS_SUCCESS';

export const ActionTypes = {
  LOAD_TASKS,
  LOAD_TASKS_SUCCESS,
  ADD_PROJECT_TASKS,
  ADD_PROJECT_TASKS_SUCCESS,
  REFRESH_TASK,
  REFRESH_TASK_SUCCESS,
  REMOVE_PROJECT_TASKS,
  REMOVE_PROJECT_TASKS_SUCCESS
};

export const loadTasks = () => ({
  type: LOAD_TASKS,
});

export const loadTasksSuccess = json => ({
  type: LOAD_TASKS_SUCCESS,
  json
});

export const addProjectTasks = projectName => ({
  type: ADD_PROJECT_TASKS,
  projectName
});

export const addProjectTasksSuccess = json => ({
  type: ADD_PROJECT_TASKS_SUCCESS,
  json
});

export const refreshTask = () => ({
  type: REFRESH_TASK
});

export const refreshTaskSuccess = (params, json) => ({
  type: REFRESH_TASK_SUCCESS,
  name: params.name,
  env: params.env,
  json
});

export const removeProjectTasks = () => ({
  type: REMOVE_PROJECT_TASKS
});

export const removeProjectTasksSuccess = (name, result) => ({
  type: REMOVE_PROJECT_TASKS_SUCCESS,
  name
});