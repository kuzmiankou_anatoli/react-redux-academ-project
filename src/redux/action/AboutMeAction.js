export const LOAD_ABOUT_ME = "LOAD_ABOUT";
export const LOAD_ABOUT_ME_SUCCESS = "LOAD_ABOUT_SUCCESS";

export const ActionTypes = {
  LOAD_ABOUT_ME,
  LOAD_ABOUT_ME_SUCCESS
};

export const loadAboutMe = () => ({
  type: LOAD_ABOUT_ME
});

export const loadAboutMeSuccess = json => ({
  type: LOAD_ABOUT_ME_SUCCESS,
  json
});