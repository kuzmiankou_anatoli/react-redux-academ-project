export const LOAD_ITEM = 'LOAD_ITEM';
export const LOAD_ITEM_SUCCESS = 'LOAD_ITEM_SUCCESS';

export const ActionTypes = {
  LOAD_ITEM,
  LOAD_ITEM_SUCCESS
};

export const loadItem = () => ({
  type: LOAD_ITEM
});

export const loadItemSuccess = json => ({
  type: LOAD_ITEM_SUCCESS,
  json
});