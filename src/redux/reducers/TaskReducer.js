import { ActionTypes } from "../action/TaskAction";

const defaultState = {
  tasks: [],
  error: null,
  loading: false
};

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case ActionTypes.LOAD_TASKS: {
      return {
        ...state, 
        error: null, 
        loading: true
      };
    }
    case ActionTypes.LOAD_TASKS_SUCCESS: {
      return {
        ...state,  
        tasks: action.json, 
        error: null, 
        loading: false
      };
    }
    
    case ActionTypes.ADD_PROJECT_TASKS: {
      const project = { name: action.projectName, environments: {} };
      const tasks = state.tasks.reduceRight((prev, curr) => {
        curr.name !== project.name && prev.push(curr);
        return prev;
      }, []);
      tasks.push(project);
      return { ...state, tasks };
    }
    case ActionTypes.ADD_PROJECT_TASKS_SUCCESS: {
      const tasks = state.tasks.map(item => ( item.name === action.json.name ? action.json : item ));
      return { ...state, tasks };
    }
    
    case ActionTypes.REFRESH_TASK: {
      return { ...state };
    }
    case ActionTypes.REFRESH_TASK_SUCCESS: {
      const tasks = state.tasks.map(item => (
        item.name === action.name 
          ? { name: item.name, environments: { ...item.environments, [action.env]: action.json } }
          : item
      ));
      return { ...state, tasks };
    }
    
    case ActionTypes.REMOVE_PROJECT_TASKS: {
      return { ...state };
    }
    case ActionTypes.REMOVE_PROJECT_TASKS_SUCCESS: {
      const tasks = state.tasks.filter( item => item.name !== action.name );
      return { ...state, tasks };
    }
    
    default: return { ...state };
  }
};

export default reducer;