import { ActionTypes } from '../action/AboutMeAction';

const defaultState = {
  name: "",
  photo: "",
  skills: [],
  overview: [],
  projects: [],
  employment: [],
  education: [],
  error: null,
  loading: false
};

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case ActionTypes.LOAD_ABOUT_ME: {
      return { 
        ...state, 
        error: null, 
        loading: true 
      }
    }
    case ActionTypes.LOAD_ABOUT_ME_SUCCESS: {
      return { 
        ...state, 
        ...action.json, 
        error: null, 
        loading: false  
      }
    }
    default: { 
      return state
    }
  }
}

export default reducer;