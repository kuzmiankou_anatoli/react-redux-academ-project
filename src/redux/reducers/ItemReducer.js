import { ActionTypes } from '../action/ItemAction';

const defaultState = {
  data: {
    name: "",
    environment: "",
    timestamps: ""
  },
  error: null,
  loading: false
};

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case ActionTypes.LOAD_ITEM: {
      return { 
        ...state, 
        error: null, 
        loading: true  
      }
    }
    case ActionTypes.LOAD_ITEM_SUCCESS: {
      return { 
        ...state,
        data: action.json, 
        error: null, 
        loading: false 
      }
    }
    default:
      return { ...state }
  }
}

export default reducer;