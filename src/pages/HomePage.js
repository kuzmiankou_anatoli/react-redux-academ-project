import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import Main from '../components/Main';

class HomePage extends Component {
  createLink(item) {
    return (
      <li className="menu-item-block" key={ item.title }>
        <a className="link btn-link" href={ item.link }>{ item.title }</a>
      </li>
    )
  }

  render() {
    const pageLinks = this.context.pageLinks;
    return (
      <Main>
        <ul className="menu side-menu">
          { pageLinks && pageLinks.map( item => this.createLink(item) ) }
        </ul>
      </Main>
    );
  }

  static contextTypes = {
    pageLinks: PropTypes.arrayOf(
      PropTypes.shape({
        link: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
      })
    )  
  }
}

export default HomePage;