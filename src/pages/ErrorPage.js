import React, {Component} from 'react';
import ErrorMessage from '../components/ErrorMessage';
import Main from '../components/Main';

class ErrorPage extends Component {
  render() {
    return (
      <Main name="Error">
        <ErrorMessage number="404" message="Page not found" />
      </Main>
    );
  }
}

export default ErrorPage;