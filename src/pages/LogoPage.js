import React, { Component } from 'react';
import Main from '../components/Main';

class LogoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: undefined
    }
  }

  componentDidMount() {
    fetch('http://localhost:9999/logo.txt')
      .then(res => res.json())
      .then(json => this.setState({ result: json.result }));
  }
  
  render() {
    const { result } = this.state;
    return (
      <Main name="Logo" loading={ !result }>
        <div>{ result }</div>
      </Main>
    )
  }
}

export default LogoPage;