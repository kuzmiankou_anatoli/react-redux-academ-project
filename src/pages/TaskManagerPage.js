import React, { Component } from 'react';
import AddComponentWindow from '../components/AddComponentWindow';
import TableBox from '../components/TableBox';
import TaskComponentManager from '../components/TaskComponentManager';
import Counter from '../components/Counter';
import Main from '../components/Main';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { loadTasks, loadTasksSuccess } from '../redux/action/TaskAction';

class TaskManagerPage extends Component {
  componentDidMount() {
    this.props.loadingData();
  }

  createTaskManager(data, number) {
    const { name, environments } = data;
    return (
      <TaskComponentManager taskName={ name } key={ name } tasks={ environments } />
    );
  }

  static ENVIRONMENTS = [
    { name: "int", title: "INT" },
    { name: "qa", title: "QA" },
    { name: "staging", title: "Staging" },
    { name: "production", title: "Production" }
  ];

  getChildContext() {
    return { 
      environments: TaskManagerPage.ENVIRONMENTS
    };
  }

  render() {
    const { loading, tasks } = this.props;
    return (
      <Main name="Task manager" loading={ loading }>
        <Counter title="Shared Services / Component" name="components-count" count={ tasks.length } />
        <AddComponentWindow />
        <TableBox>
          { tasks.map( (item, number) => this.createTaskManager(item, number) ) }
        </TableBox>
      </Main>
    );
  }

  static childContextTypes = {
    environments: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      title: PropTypes.string
    }))
  }
}

const mapStateToProps = state => {
  const { TaskReducer } = state;
  return { ...TaskReducer };
}

const mapDispatchToProps = dispatch => ({ 
  loadingData: () => {
    dispatch(loadTasks());
    fetch('http://localhost:9999/data/projects')
      .then(res => res.json())
      .then(json => dispatch(loadTasksSuccess(json)));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskManagerPage);