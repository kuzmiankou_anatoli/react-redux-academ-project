import React, {Component} from 'react';
import Skills from '../components/views/SkillsView';
import ProjectView from '../components/views/ProjectView';
import EmploymentView from '../components/views/EmploymentView';
import EducationView from '../components/views/EducationView';
import Main from '../components/Main';
import * as PropTypes from 'prop-types';
import { loadAboutMe, loadAboutMeSuccess } from '../redux/action/AboutMeAction';
import { connect } from 'react-redux';

class AboutMePage extends Component {
  componentDidMount() {
    this.props.loadingData();
  }

  createParagraph(text, number) {
    return (
      <p key={ number }>{ text }</p>
    );
  }

  createProject(props) {
    return (
      <ProjectView key={ props.link } data={ props } />
    );
  }

  createEmployment(props) {
    return (
      <EmploymentView key={ props.start + props.end } data={ props } />
    );
  }

  createEducation(props) {
    return (
      <EducationView key={ props.start + props.end } data={ props } />
    );
  }

  render() {
    const { name, skills, overview, projects, employment, education, loading } = this.props;
    return (
      <Main className="about-me" name="About me" loading={ loading }>
        <h2>{ name }</h2>
        <Skills values={ skills } />
        <h3>Overview:</h3>
        <div>{ overview.map( (item, number) => this.createParagraph(item, number)) }</div>
        <h3>My Projects:</h3>
        { projects.map( (item, number) => this.createProject(item, number) ) }
        <h3>Employment History</h3>
        { employment.map( item => this.createEmployment(item) ) }
        <h3>Education:</h3>
        { education.map( item => this.createEducation(item) ) }
      </Main>
    );
  }

  static propsTypes = {
    name: PropTypes.string,
    skills: PropTypes.arrayOf(PropTypes.string),
    overview: PropTypes.arrayOf(PropTypes.string),
    projects: PropTypes.array,
    employment: PropTypes.array,
    education: PropTypes.array
  };
}

const mapStateToProps = state => {
  const { AboutMeReducer } = state;
  return {
    ...AboutMeReducer
  };
}

const mapDispatchToProps = dispatch => ({
  loadingData: () => {
    dispatch(loadAboutMe());
    fetch('http://localhost:9999/data/about-me.json')
      .then(res => res.json())
      .then(json => dispatch(loadAboutMeSuccess(json)));
  } 
});

export default connect(mapStateToProps, mapDispatchToProps)(AboutMePage);