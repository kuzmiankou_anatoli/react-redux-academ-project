import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import Main from '../components/Main';
import { connect } from 'react-redux';
import { loadItem, loadItemSuccess } from '../redux/action/ItemAction';

class ItemPage extends Component {
  componentDidMount() {
    const { location } = this.props;
    this.props.loadingData(location.pathname);
  }
  
  createDetail(key, value) {
    return [
      <dt key="dt">{ key }</dt>,
      <dd key="dd">{ value }</dd>
    ]
  }

  createDetails(items) {
    const result = [];
    for(let key in items) {
      result.push(this.createDetail(key, items[key]));
    }
    return result;
  }

  render() {
    const { name, environment, timestamps, loading } = this.props.data;
    const { params } = this.props.match;
    return (
      <Main name={ `${ params.name }(${ params.environment })` } loading={ loading }>
        <h2>{ `${ name } / ${ environment } / ${ timestamps ? timestamps : "now" }` } </h2>
        <h3>Details:</h3>
        <ul className="list-details">{ this.createDetails(this.props.data) }</ul>
      </Main>
    );
  }

  static propTypes = {
    data: PropTypes.objectOf(PropTypes.string)
  };
}

const mapStateToProps = state => {
  const { ItemReducer } = state;
  return {
    ...ItemReducer
  }
};

const mapDispatchToProps = dispatch => ({ 
  loadingData: pathname => {
    dispatch(loadItem());
    fetch(`http://localhost:9999/data${ pathname }`)
      .then(res => res.json())
      .then(json => dispatch(loadItemSuccess(json)));
  } 
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemPage);