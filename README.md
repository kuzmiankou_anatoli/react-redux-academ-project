### Instal modules
< npm install >

### Start server
< npm node server/server.js >

### Start developer version
< npm start >

### Build production version
< npm run build >

### Start production version
You may serve it with a static server:
< serve -s build >