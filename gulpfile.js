const gulp = require('gulp');
const less = require('gulp-less');

gulp.task('less', () => {
  return (gulp.src(__dirname + '/src/**/*.less')
    .pipe(less())
    .pipe(gulp.dest(__dirname + '/src')));
});

gulp.task('watch', ['less'], () => {
  gulp.watch(__dirname + '/src/**/*.less', ['less']);
});